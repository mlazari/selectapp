import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import {
  Select,
  Option
} from 'react-native-chooser';


export default class SelectApp extends Component {

  constructor(props) {
    super(props);

    this.state = {
      selected: 'first',
      defaultText: 'First'
    };

    this.selectSecond = this.selectSecond.bind(this);
  }

  selectSecond() {
    this.setState({
      selected: 'second',
      defaultText: 'Second'
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Select
          selected={this.state.selected}
          defaultText={this.state.defaultText}
        >
          <Option value='first'>First</Option>
          <Option value='second'>Second</Option>
        </Select>
        <TouchableOpacity
          style={styles.button}
          onPress={this.selectSecond}
        >
          <Text>Select second</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  button: {
    marginTop: 30,
    padding: 10
  }
});

AppRegistry.registerComponent('SelectApp', () => SelectApp);
